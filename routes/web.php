<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Main route
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Home
Route::get('/home', 'HomeController@index')->name('home');

// Post
Route::post('/notes', 'NoteController@store');
Route::get('/notes', 'NoteController@index');
Route::get('/notes/create', 'NoteController@create');
Route::get('/notes/my-notes', 'NoteController@myNotes');  // another custom method
Route::get('/notes/archived', 'NoteController@archivednotes'); // another custom method
Route::get('/notes/{note_id}', 'NoteController@show');
Route::get('/notes/{note_id}/edit', 'NoteController@edit');
Route::put('/notes/{note_id}', 'NoteController@update');
Route::delete('/notes/{note_id}', 'NoteController@destroy');
Route::put('/notes/{note_id}/archive', 'NoteController@archive');  // 


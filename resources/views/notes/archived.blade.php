@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>List of all archived notes</h1>
        </div>
        <div class="col-md-8">
          @foreach ($archived_notes as $note)
            @if (!$note->is_active)
            <div class="row">
              <div class="card mb-3" style="width:100%;">
                <div class="card-body">
                  <h3 class="card-title">
                    <a id="{{ $note->id }}" href="/notes/{{ $note->id }}">{{ $note->title }}</a>
                    <small class="badge badge-pill badge-secondary">{{ $note->is_active ? '' : 'archived' }}</small>
                  </h3>
                  <p class="card-text">{{ $note->content }}</p>
                </div>
              </div>
            </div>
            @endif
          @endforeach
        </div>
    </div>
</div>
@endsection

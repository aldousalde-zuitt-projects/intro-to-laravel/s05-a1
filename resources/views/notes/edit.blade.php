@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>Update an existing note</h1>
        </div>
        <div class="col-md-8">
          <form method="POST" action="/notes/{{$note->id}}">
            @method('PUT')
            @csrf
            <div class="form-group">
              <label for="title">Title:</label>
              <input type="text" class="form-control" id="title" name="title" value="{{ $note->title }}">
            </div>
            <div class="form-group">
              <label for="content">Content:</label>
              <input type="text" class="form-control" id="content" name="content" value="{{ $note->content }}">
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="/notes/{{ $note->id }}" class="btn btn-secondary">Cancel</a>
          </form>
        </div>
    </div>
</div>
@endsection